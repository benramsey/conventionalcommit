# Conventional commit
 This  package will use .git/hooks/commit-msg  to check if git commit -m "Your message" is conventional commit compliant.

## Instalation
Add to composer.json
```
"scripts": {
    "post-install-cmd": [
      "SyncHot\\ConventionalCommit\\Services\\ComposerBuild::copyHooks"
    ],
    "post-update-cmd": [
      "SyncHot\\ConventionalCommit\\Services\\ComposerBuild::copyHooks"
    ]
  }
```

run


```
composer require --dev synchot/conventionalcommit
```

### How to run

On windows machines please select git-bash as default console.

```
git commit -m "Some message"
```

### Something went wrong
In Your project  do :

```
cat .git/hooks/commit-msg
```

If it has the same value as in vendor/synchot/conventionalcommit/resources/commit-msg 
Tou should be good to go, else try to copy this value.

### User defined config
Create /config/packages/conventionalcommit.yaml

with contents of  

/vendor/synchot/conventionalcommit/config/conventionalcommit.yaml

Modify all values to suit Your projects needs.

### REMOVING THIS PLUGIN
```
cd <your project dir>
cd .git/hooks
rm commit-msg
```
### CHANGELOG
[https://bitbucket.org/SyncHot/conventionalcommit/src/master/CHANGELOG.md](https://bitbucket.org/SyncHot/conventionalcommit/src/master/CHANGELOG.md)

### Todos
- Refactor
- Write Tests

License
----

MIT Jędra Marcin