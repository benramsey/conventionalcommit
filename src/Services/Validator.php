<?php

namespace SyncHot\ConventionalCommit\Services;

use Symfony\Component\Yaml\Exception\ParseException;
use Webmozart\Assert\Assert;

class Validator
{

    private static $configuration;

    /**
     * @param string $value
     * @return void
     */
    public static function validate(string $field, ?string $value)
    {
        self::setConfiguration();

        $validation = self::$configuration[$field];

        if (is_null($value) || (!empty($validation['optional']) && empty($value))) {
            return;
        }


        if (!empty($validation['regex'])) {
            Assert::regex($value, $validation['regex'], 'Scope must be compatible with regex ' . $validation['regex']);
        }
        if (!empty($validation['length']['min'])) {
            Assert::minLength($value, $validation['length']['min'], $validation['errorMessage']);
        }
        if (!empty($validation['length']['max'])) {
            Assert::maxLength($value, $validation['length']['max'], $validation['errorMessage']);
        }
    }

    /**
     * @return void 
     * @throws ParseException 
     */
    private static function setConfiguration()
    {
        if (!isset(self::$configuration)) {
            self::$configuration = ConfigurationReader::read();
        }
    }
}
