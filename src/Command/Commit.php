<?php

namespace SyncHot\ConventionalCommit\Command;

use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;
use SyncHot\ConventionalCommit\DataObjects\CommitMessage;
use SyncHot\ConventionalCommit\Exceptions\FileDumpException;
use SyncHot\ConventionalCommit\Services\ConfigurationReader;

class Commit
{
    /**
     * @param CommitMessage $commitMessage 
     * @return void 
     */
    public static function createConventionalCommit(CommitMessage $commitMessage, Input $input): void
    {
        try {
            (new Filesystem())->dumpFile($input->getArgument(ConventionalCommit::COMMIT_MESSAGE_FILE_LOCATION), self::buildCommitMessageString($commitMessage));
        }catch(IOException $ex){
            throw new FileDumpException($ex->getMessage(), $ex->getCode());
        }
    }

    /**
     * @param Input $input 
     * @return string 
     * @throws InvalidArgumentException 
     * @throws IOException 
     */
    public static function getConventionalCommit(Input $input): string
    {
        $fileLocation = $input->getArgument(ConventionalCommit::COMMIT_MESSAGE_FILE_LOCATION);
        
        if((new FileSystem())->exists($fileLocation)){
            return file_get_contents($fileLocation);
        }

        return '';
    }

    /**
     * @param CommitMessage $commitMessage 
     * @return string 
     */
    private static function buildCommitMessageString(CommitMessage $commitMessage): string 
    {
        return  (string) $commitMessage->getSubject() . "\r\n" . $commitMessage->getBody() . "\r\n" . $commitMessage->getFooter();
    }


   /**
    * @param string $inputCommitMessage 
    * @return bool 
    */
    public static function isValidMessage(string $inputCommitMessage): bool
    {
        return preg_match(ConfigurationReader::read()['conventionalCommitRegex'], $inputCommitMessage);
    }
}
