<?php
namespace SyncHot\ConventionalCommit\DataObjects\Enums;
use MyCLabs\Enum\Enum;


class CommitType extends Enum
{
    private const FEATURE = 'feat';
    private const FIX     = 'fix';
    private const BUILD   = 'build';
    private const CHORE   = 'chore';
    private const CI      = 'ci';
    private const DOCS    = 'docs';
    private const STYLE   = 'style';
    private const REFACTOR = 'refactor';
    private const PERFORMANCE = 'perf';
    private const TEST        = 'test';
}