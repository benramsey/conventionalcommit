<?php
namespace SyncHot\ConventionalCommit\DataObjects;

class CommitMessage {
    
    private $subject;
    private $body;
    private $footer;


    public function __construct(CommitSubject $subject, CommitBody $body, CommitFooter $footer)
    {
        $this->setSubject($subject);
        $this->setBody($body);
        $this->setFooter($footer);
    }   

    public function setSubject(CommitSubject $subject){
        $this->subject = $subject;
    }

    public function setBody(CommitBody $body) {
        $this->body = $body;
    }

    public function setFooter(CommitFooter $footer){
        $this->footer = $footer;
    }
    /**
     * @return CommitSubject
     */
    public function getSubject(): CommitSubject
    {
        return $this->subject;
    }
    /**
     * @return CommitBody
     */
    public function getBody(): CommitBody
    {
        return $this->body;
    }

    /**
     * @return CommitFooter
     */
    public function getFooter(): CommitFooter
    {
        return $this->footer;
    }
    
}